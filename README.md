Garcia Zeman Carlos Luciano MPWAR

Entregable Docker Compose
Enunciado
Montar el esqueleto de un proyecto con Docker Compose que contendrá los siguientes servicios:
- redis
- Actuará como base de datos
- Imagen propia basada en “redis:5”
- Añadirá la instrucción LABEL maintainer=“Salle MPWAR Maintainers”
- http
- Recibirá las peticiones de http
- Imagen propia basada en “nginx:stable”
- Añadirá la instrucción LABEL maintainer=“Salle MPWAR Maintainers”
- php
- Ejecutará código php
- Imagen propia basada en “php:7.3-fpm”
- Añadirá la instrucción LABEL maintainer=“Salle MPWAR Maintainers”
Este proyecto publicará un endpoint HTTP accesible en el host mediante la URL
http://localhost:8085/php-status.php y que devolverá la respuesta de ejecutar este script de
PHP:
1
<?php
phpinfo();
Consideraciones
Entregar un repositorio nuevo, en Gitlab, que incluya la siguiente estructura:
infrastructure/environments
Las variables de entorno estarán definidas en dos ficheros diferentes:
- /infrastructure/environments/development
- APP_ENV=dev
- APP_DEBUG=true
- /infrastructure/environments/production
- APP_ENV=production
- APP_DEBUG=false
Configuraremos Docker Compose con las variables de entorno de development para este
ejercicio.
infrastructure/docker
Los ficheros relacionados con Docker y sus imágenes se colocarán en el directorio
/infrastructure/docker/
2
/path/to/project
├─ infrastructure/
│ ├─ docker/
│ └─ environments/
│  ├─ development
│  └─ production
├─ src/
│ └─ … PHP code …
├─ README.md
└─ docker-compose.yml